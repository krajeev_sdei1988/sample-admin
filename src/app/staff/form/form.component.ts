import { Component, OnInit, ViewChild } from '@angular/core';

import {Router, ActivatedRoute, Params} from '@angular/router';

import { StaffService } from "../staff.service";
//import { RolesService } from "../../roles/roles.service";
import { NotificationsService } from '../../shared/notifications.service';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { DatePickerOptions } from '../../shared/date-picker-options';
import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-form',
  providers: [],
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent  extends BaseForm implements OnInit {
  roles:any;
  userType: any  = "";
  phone_number: '';
  confirmPassword: any = "";
  datePickerOptions = DatePickerOptions;
  preferredCountries = ['us', 'au', 'ru', 'gb'];
  language:any;
  launguages: any;
  disableAdmin: any = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: StaffService,
   // private rolesService: RolesService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {

    this.item.companyId=this.LoggedUser.companyId;
    this.item.createdBy=this.LoggedUser.id;
    this.item.creatorType=this.LoggedUser.userType;
    
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        this.itemID = params['id'];
        if(this.itemID){
          this.isEditable = true;
          this.getItem();
        }
        
      });
     this.getRoles();
      this.language = 'en';

     if(this.itemID==null){
      this.notificationsService.updateBreadCrumbs([{lable:'Staff Members',url:`/staff`},{lable:'Add',url:`/staff/new`}]);
    }else{
      this.notificationsService.updateBreadCrumbs([{lable:'Staff Members',url:`/staff`},{lable:'Edit',url:`/staff/${this.itemID}/edit`}]);
    }
  }

  getItem() {
     this.busy = this.klassService.getItem(this.itemID, this.language,"admin/getData").then((res) => {
      if(!res){
        this.router.navigate([`/staff`]);
        return true;
      }

      this.item = res.data;
      this.item.password = "";
      // if(this.item.hasOwnProperty('role')){
      //     if(this.item.role.role === 'Super Admin'){
      //       this.disableAdmin = true;
      //     }
      // }

    });
  }
  // save(){
  //   console.log(this.user)
  // }

  getRoles() {
    this.roles = [];
    let filters = { "page": "1", "count": "100", "sort": "created_date" };

    this.roles=[ {value:2,name:'Admin'},
              {value:3,name:'Sub Admin'}
   
            ];
    // this.busy = this.rolesService.getItems(this.language, 'roles').then((res) => {
    //   if(!res)
    //     return true;

    //   this.roles = res.data;
    //   // this.roleID = this.roles[0]._id;
    //   this.item.role_name = this.roles[0].role;

    //   if(this.itemID) {
    //     this.getItem();
    //   }
    // });
  }


  createItem() {
    console.log("this.item====>",this.item)
    this.busy = this.klassService.addItem(this.item, this.language,"admin/create").then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      this.router.navigate(['/staff']);
      this.notificationsService.notify('success', this.constantMessages.staffCreated);
    });
  }

  updateItem() {
    if(!this.item || this.item.password.length <= 0){
      delete this.item.password
    }
    console.log("this.item",this.item)
    this.item.user_id = this.itemID;
    this.busy = this.klassService.updateItem(this.itemID, this.item, this.language, 'admin/edit').then((res) => {
      if(!res)
        return true;
      this.item.password = "";
      this.confirmPassword = "";
     
      this.form.form.markAsPristine();
      this.notificationsService.notify('success', this.constantMessages.staffUpdated);
    });
  }

  updateRoleName(event) {
    // this.item.role_name = event.target['options'][event.target['options'].selectedIndex].text;
    // this.item.role = this.roles.filter(role=> role._id === this.roleID)[0];
    // let role =this.roles.filter(role=> role._id === this.roleID)
    this.markDirty();
  }
}
