import { NgModule } from '@angular/core';

import { AppSharedModule } from '../shared/app-shared.module';
import { StaffRoutingModule } from './staff.routing';

import { StaffComponent } from './staff.component';
import { FormComponent } from './form/form.component';
import { ListingComponent } from './listing/listing.component';

import { StaffService } from "./staff.service";
// import {NgxMaskModule} from 'ngx-mask'

@NgModule({
  imports: [
    AppSharedModule,
    StaffRoutingModule,
//   NgxMaskModule.forRoot()
	
  ],
  providers: [StaffService],
  declarations: [StaffComponent, FormComponent, ListingComponent]
})
export class StaffModule { }
