import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params, Route } from '@angular/router';
import { Listing } from '../../shared/listing';

import { StaffService } from "../staff.service";
import { NotificationsService } from '../../shared/notifications.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent extends Listing implements OnInit {
  language :any;
  public itemsTotal = 100;  
  constructor(
    private klassService: StaffService,
    private notificationsService: NotificationsService,
    private _router: Router,
    private toastr: ToastrService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';
    this.getItems();
    this.notificationsService.updateBreadCrumbs([{lable:'Staff Members',url:`/planets/?search=""`}]);

    //this.isAdmin = localStorage.getItem('isAdmin') === 'true';


  }

public filterItems1() {
    let reg = new RegExp('[a-zA-Z][a-zA-Z ]+');

    if ((this.searchTerm.length === 0 || this.searchTerm.length > 2)) {
      if(this.searchTerm.length > 2  && reg.test(this.searchTerm)){ 
        if(this.tmpItems.length == 0)
        this.tmpItems = this.items;
      this.items = this.tmpItems.filter(_item => _item.firstName.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1    )

      console.log(this.items)
      }
      if(this.searchTerm.length === 0){
        if(this.tmpItems.length == 0)
        this.tmpItems = this.items;
      this.items = this.tmpItems.filter(_item => _item.firstName.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1 )
      }
    }
}
 
  public searchEquipment(event) { 
    // this.searchTerm = this.searchTerm.trimLeft();
    this.activePage = 1;
    this.filterItems1();
  }

onSortOrder(e){
    this.filterParams.sortBy = this.sortBy
    this.filterParams.sortType =  e
    this.getItems();
  }

 

   getItems() {
     this.filterParams.companyId=this.LoggedUser.companyId; 
    this.busy = this.klassService.getXItems(this.filterParams, this.language, 'admin/list').then((res) => {
      if(!res)
        return true;
      console.log("=====>",res)
      this.items = res.data;
      this.tmpItems = this.items;
      this.itemsTotal = res.count;
    });
  }
public onPageChange(event) {
    this.filterParams.count = event.rowsOnPage;
    this.filterParams.page = event.activePage;
    this.getItems();
  }
  removeItem(item) {


    Swal({
      title: 'Are you sure?',
      text: 'You will not be able to recover this staff!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {


      let obj = { _id: item._id}
     // console.log("obj===",)
    this.busy = this.klassService.removeItem(obj, this.language, `admin/delete/${item._id}`).then((res) => {
      if(!res)
        return true;

      this.item = null;
      this.getItems();
      this.notificationsService.notify('success', this.constantMessages.staffDeleted);
     
    });
        
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal(
          'Cancelled',
          'Your staff is safe :)',
          'error'
        )
      }
    })
    
    
  }

  toggleStatus(item) {
    let obj = { user_id: item._id, is_active: item.is_active }
    this.busy = this.klassService.toggleStatus(obj, this.language, 'admin/user').then((res) => {
      if(!res)
        return true;

      this.notificationsService.notify('success', this.constantMessages.staffStatus);
    });
  }

 

  public filterItems() {
    if(this.searchTerm.length === 0 || this.searchTerm.length>2){
    if (this.tmpItems.length == 0)
      this.tmpItems = this.items;
    this.items = this.tmpItems.filter(_item =>
       _item.name.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1 ||
       _item.email.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1 ||
       _item.mobile_number.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1
    );
  }
}
}
