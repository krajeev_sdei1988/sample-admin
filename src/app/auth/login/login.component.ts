import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, Route } from '@angular/router';
import { RouterModule } from '@angular/router';
import { LoginService } from './login.service';

import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  busy: Promise<any>;
  public user = { username: "", password: "" };
  public msg = '';
  public isForgotPassword: boolean = false;
  public emailNew: string;

  constructor(
    private _service: LoginService,
    private _router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    localStorage.clear();
    
  }

  forgotPassword(n=0){
    if(n==0) {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
       }else{
          
            $("#recoverform").slideUp();
             $("#loginform").fadeIn();
       }
  }

  login() {
    this.busy = this._service.login(this.user).then(
      (res: any) => {
        let data = res;
        localStorage.setItem('token',data.userInfo.myToken);
       localStorage.setItem('UserInfo', JSON.stringify(data.userInfo));
        this._router.navigate(['/']);
        
        
      },
      (error) => {
        if(error.headers._headers.get('content-type')[0] == "application/json; charset=utf-8") {
          this.toastr.error(error.json().message);
        } else {
          this.toastr.error('you are not able to login. Please try later.');
        }
      }
    );
  }

}
