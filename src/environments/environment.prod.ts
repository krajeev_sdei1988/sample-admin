export const environment = {
  production: true,
  envName: 'liveProduction',
  config: {
    APP_NAME: "Picture Line Rental",
    SOCKET_URL: "http://34.225.208.103:4183",
    BASE_URL: "http://34.225.208.103:4183/",
    API_URL: "http://34.225.208.103:4183/",
    uploadPath: "https://swapi.co/uploadedFiles/"
  }
};
